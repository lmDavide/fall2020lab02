//David Tran 1938381

package lab02.eclipse;

public class BikeStore {
	
	public static void main(String[]args) {
		System.out.println("Welcome to our bike store!");
		System.out.println("Here is the list of bikes we currently have! :D");
		System.out.println("");
		Bicycle[] bicycleList = new Bicycle[4];
		Bicycle newBicycle = new Bicycle("lmSlow", 18, 20.0);
		Bicycle newBicycle1 = new Bicycle("Hello", 21, 25.0);
		Bicycle newBicycle2 = new Bicycle("Goodbye", 24, 30.0);
		Bicycle newBicycle3 = new Bicycle("lmFast", 27, 35.0);
		bicycleList[0] = newBicycle;
		bicycleList[1] = newBicycle1;
		bicycleList[2] = newBicycle2;
		bicycleList[3] = newBicycle3;
		System.out.println(bicycleList[0].toString());
		System.out.println(bicycleList[1].toString());
		System.out.println(bicycleList[2].toString());
		System.out.println(bicycleList[3].toString());
	}
	
}