//David Tran 1938381

package lab02.eclipse;

public class Bicycle {
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;
	
	public Bicycle(String manufacturerInput, int numberGearsInput, double maxSpeedInput) {
		this.manufacturer = manufacturerInput;
		this.numberGears = numberGearsInput;
		this.maxSpeed = maxSpeedInput;
	}
	
	public String getManufacturer() {
		return this.manufacturer;
	}

	public int getNumberGears() {
		return this.numberGears;
	}
	
	public double getMaxSpeed() {
		return this.maxSpeed;
	}
	
	public String toString() {
		return ("Manufacturer: " + this.manufacturer + ", Number of Gear: " + this.numberGears + ", Maximum Speed: " + this.maxSpeed + " .");
	}
}